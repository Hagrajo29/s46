// Base Imports
import React, {useState, useEffect} from 'react';

// Bootstrap Components
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

export default function Course(props) {


    let course = props.course;


    const [isDisabled, setDisabled] = useState(0);
    const [seats, setSeats] = useState(30);

    useEffect(()=> {
        if (seats === 0) {
            setIsDisabled (true);
        }

    },[seats]);

    return (
        <Card>
            <Card.Body>
                <Card.Title>{course.name}</Card.Title>
                <h6>Description</h6>
                <p>{course.description}</p>
                <h6>Price</h6>
                <p>{course.price}</p>
                <h6>Enrollees</h6>
                <p>{count} Enrollees</p>
                <Button variant="primary" onClick ={enroll}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}
